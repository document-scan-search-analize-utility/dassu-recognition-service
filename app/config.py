from flask import Flask
import tempfile

UPLOAD_FOLDER = '/usr/app/tmp'
TEMP_FOLDER = tempfile.mkdtemp()

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['TEMP_FOLDER'] = TEMP_FOLDER
app.config['TESSERACT_CONFIG'] = "--oem 3 --psm 6 -l rus -c textord_heavy_nr=1 -c enable_new_segsearch=1"
app.config['CLASSIFY_MODEL_PATH'] = 'models/resinception_acc97.h5'
