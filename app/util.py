import subprocess

import ocrmypdf
import pdf2image
from pathlib import Path

import logging
import cv2

from PIL import Image
from pytesseract import pytesseract, Output

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)

ALLOWED_EXTENSIONS = {'pdf'}


def preprocess_ocrmypdf(input_path, output_path):
    logger.debug(f"Start preprocess ocrmypdf file {input_path} to {output_path}")
    ocrmypdf.ocr(input_path, output_path, deskew=True, clean=True, clean_final=True,
                 tesseract_timeout=0, rotate_pages=True, remove_background=True, skip_text=True)
    logger.debug(f"End preprocess ocrmypdf file {input_path} to {output_path}")


def preprocess_convert(input_path, output_path):
    logger.debug(f"Start preprocess convert file {input_path} to {output_path}")
    pages = pdf2image.convert_from_path(input_path, dpi=300)

    page_paths = []
    for i, page in enumerate(pages):
        page_path = output_path/Path('{}.png'.format(i))
        page_paths.append(page_path)
        page.save(page_path)
    logger.debug(f"End preprocess convert file {input_path} to {output_path}")
    return page_paths


def preprocess_textcleaner(input_path, output_path):
    logger.debug(f"Start preprocess textcleaner file {input_path} to {output_path}")
    result = subprocess.run(f'textcleaner -g -e stretch -f 30 -o 40 -s 1 "{input_path}" "{output_path}"',
                   stderr=subprocess.PIPE, shell=True)
    if not result.returncode == 0:
        raise Exception(f"Unable to preprocess textcleaner file {input_path} to {output_path}")
    logger.debug(f"End preprocess textcleaner file {input_path} to {output_path}")


def preprocess_opencv(input_path, output_path):
    logger.debug(f"Start preprocess opencv file {input_path} to {output_path}")
    image_input = cv2.imread(input_path, cv2.IMREAD_GRAYSCALE)
    kernel_v = cv2.getStructuringElement(cv2.MORPH_RECT, (1, image_input.shape[0] // 100))
    kernel_h = cv2.getStructuringElement(cv2.MORPH_RECT, (image_input.shape[1] // 100, 1))
    kernel_e3 = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
    kernel_e2 = cv2.getStructuringElement(cv2.MORPH_CROSS, (2, 2))
    _, image_output = cv2.threshold(image_input, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    image_output = cv2.dilate(image_output, kernel_e3, iterations=1)
    image_lines_v = cv2.morphologyEx(image_output, cv2.MORPH_OPEN, kernel_v, iterations=3)
    image_lines_h = cv2.morphologyEx(image_output, cv2.MORPH_OPEN, kernel_h, iterations=3)
    image_output = cv2.addWeighted(image_lines_v, 1, image_lines_h, 1, 0)
    image_output = cv2.dilate(image_output, kernel_e2, iterations=1)
    image_output = cv2.bitwise_or(image_input, image_output)
    image_output = cv2.morphologyEx(image_output, cv2.MORPH_CLOSE, kernel_e2, iterations=1)
    image_output = cv2.fastNlMeansDenoising(image_output, h=20, templateWindowSize=19, searchWindowSize=31)
    _, image_output = cv2.threshold(image_output, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imwrite(output_path, image_output)
    logger.debug(f"End preprocess opencv file {input_path} to {output_path}")


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def extract_words(img, custom_config=None):
    data = pytesseract.image_to_data(img, output_type=Output.DICT, config=custom_config)
    # data = pytesseract.image_to_data(img, output_type=Output.DICT)
    n_boxes = len(data['text'])

    words = [
        {
            'text': data['text'][i],
            'left': data['left'][i],
            'top': data['top'][i],
            'right': data['left'][i] + data['width'][i],
            'bottom': data['top'][i] + data['height'][i],
            'id': k
        } for k, i in enumerate(filter(lambda i: len(data['text'][i]) > 0, range(n_boxes)), 1)
    ]
    return words


def get_image(file_path):
    img = Image.open(file_path)
    return img
