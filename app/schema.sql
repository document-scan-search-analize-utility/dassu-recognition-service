CREATE TABLE IF NOT EXISTS documents (
    file_id INTEGER PRIMARY KEY,
    filename TEXT NOT NULL,
    hash TEXT NOT NULL,
    category INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS pages (
    page_id INTEGER PRIMARY KEY,
    file_id INTEGER,
    hash TEXT NOT NULL,
    filename TEXT NOT NULL,
    annotated_data TEXT NOT NULL
);