FROM ubuntu:18.04

RUN apt update && apt install software-properties-common -y

#RUN echo "deb http://cz.archive.ubuntu.com/ubuntu eoan main universe" >> /etc/apt/sources.list
RUN apt-add-repository ppa:~alex-p/tesseract-ocr-devel
RUN apt update && apt install -y tesseract-ocr poppler-utils libxext-dev libsm-dev libxrender-dev \
    unpaper \
    ghostscript \
    qpdf \
    imagemagick \
    bc \
    tesseract-ocr-rus \
    tesseract-ocr-chi-sim

RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt update && apt install -y python3.8 python3.8-dev python3.8-venv curl

RUN ln -s /usr/bin/python3.8 /usr/bin/python
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python get-pip.py

RUN sed -i '/disable ghostscript format types/,+6d' /etc/ImageMagick-6/policy.xml
WORKDIR /usr/app

COPY ./scripts/textcleaner /usr/bin/textcleaner
COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY . .

RUN chmod +x /usr/bin/textcleaner

ENTRYPOINT ["python", "-u", "main.py"]