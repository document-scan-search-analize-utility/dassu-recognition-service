from collections import defaultdict
from io import BytesIO
from pathlib import Path

from flask import request, redirect, flash, jsonify
from werkzeug.utils import secure_filename

from app.util import allowed_file, \
    preprocess_convert, preprocess_ocrmypdf, extract_words, get_image, preprocess_textcleaner, preprocess_opencv
from app.config import app
from app.db import init_db, get_db, query_db
from hashlib import sha512
import uuid
from keras.models import load_model
import numpy as np
from PIL import Image
import tempfile
import base64
import os

import logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)


def preprocessing_pipeline(save_path, filename):
    preprocessed_path = os.path.join(app.config['TEMP_FOLDER'], filename)
    try:
        preprocess_ocrmypdf(save_path, preprocessed_path)
    except Exception as e:
        logger.error(f"Unable to preprocess ocrmypdf file {filename}. Error: {e}.")
    save_path = preprocessed_path


    temp_dir = tempfile.mkdtemp(suffix='convert_')
    preprocessed_path = os.path.join(temp_dir, os.path.basename(filename)[:-4])
    if not os.path.exists(preprocessed_path):
        os.mkdir(preprocessed_path)

    try:
        preprocess_convert(save_path, preprocessed_path)
    except Exception as e:
        logger.error(f"Unable to preprocess convert file {filename}. Error: {e}.")
    save_path = preprocessed_path
    page_paths = list(Path(save_path).glob('**/*.png'))

    class_label, class_value = predict_label(page_paths)

    for i in range(len(page_paths)):
        page_path = page_paths[i]
        save_path = str(page_path)[:-3] + 'tc.png'
        preprocess_textcleaner(page_path, save_path)
        page_path = save_path
        save_path = str(page_path)[:-6] + 'cv.png'
        preprocess_opencv(page_path, save_path)
        page_paths[i] = save_path

    return page_paths, class_label, class_value


def predict_label(page_paths):
    predictions = defaultdict(list)

    class_label = -1
    class_value = 0

    for page_path in page_paths:
        img = Image.open(page_path)
        max_size = (224,224)
        im_resized = img.resize(max_size)

        pred = class_model.predict(np.expand_dims(np.asarray(im_resized).reshape(224, 224, 3), axis=0))[0]
        logger.debug(f"Predictions from model: {pred}")
        class_label = np.argmax(pred)
        logger.debug("For {} predict {} with {}".format(page_path, class_label, pred[class_label]))
        predictions[class_label].append(pred[class_label])

    for class_label_pred, class_values_pred in predictions.items():
        if np.mean(class_values_pred) > class_value:
            class_value = np.mean(class_values_pred)
            class_label = class_label_pred

    return int(class_label), float(class_value)


@app.route('/upload', methods=['POST'])
def upload():
    if 'file' not in request.files:
        flash('No file part')
        return jsonify({'error': 'no file part', 'status': 400})
    f = request.files['file']
    if f.filename == '':
        flash('No selected file')
        return jsonify({'error': 'no selected file', 'status': 400})
    if f and allowed_file(f.filename):
        filename = secure_filename(f.filename)
        save_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        f.save(save_path)

        page_paths, class_label, class_value = preprocessing_pipeline(save_path, filename)
        logger.debug(f"FInal prediction: {class_label}: {class_value}")

        filename_hash = str(sha512(filename.encode()).hexdigest())

        try:
            with app.app_context():
                conn = get_db()
                cur = conn.execute('insert into documents (filename, category, hash) values (?, ?, ?)', (
                    save_path,
                    class_label,
                    filename_hash
                ))

                conn.commit()
                file_id = cur.lastrowid

                page_ids = []
                for page_path in page_paths:
                    page_hash = str(sha512(str(page_path).encode()).hexdigest())
                    cur = conn.execute('insert into pages (file_id, filename, hash, annotated_data) values (?, ?, ?, ?)', (
                        file_id,
                        str(page_path),
                        page_hash,
                        ""
                    ))

                    conn.commit()
                    page_id = cur.lastrowid
                    page_ids.append("{}:{}:{}".format(page_id, file_id, page_hash))

                cur.close()
        except Exception as e:
            logger.error(f"Unable to save {filename} into db. Error: {e}.")
            return jsonify({'error': 'unable to save', 'status': 400})

        return jsonify({
            'document_id': '{}:{}'.format(file_id, filename_hash),
            'category': class_label,
            'metric': class_value,
            'pages': page_ids
        })


@app.route('/<document_hash>/pages/<page_hash>', methods=['GET'])
def recognize_page(document_hash, page_hash):
    try:
        file_id, file_hash = document_hash.split(':', 1)
    except ValueError:
        flash('Incorrect file hash')
        return jsonify({
            'error': 'file not found',
            'status': 400
        })
    try:
        page_id, file_id, page_hash = page_hash.split(':')
    except ValueError:
        flash('Incorrect page hash')
        return jsonify({
            'error': 'page not found',
            'status': 400
        })


    try:
        with app.app_context():
            page_row = query_db('select filename, hash, annotated_data from pages where file_id = ? and page_id = ?',
                                (file_id, page_id), one=True)
            logger.debug(f'Results from db: {page_row}')
            if page_row is None or 'hash' not in page_row or page_row['hash'] != page_hash or \
                    'annotated_data' not in page_row:
                raise Exception("Incorrect hash")
    except Exception as e:
        logger.error(f"Unable to load {page_hash} from db. Error: {e}.")
        flash(f'Row for file {page_hash} not found')
        return jsonify({
            'status': 400,
            'error': 'page row not found'
        })

    filepath = page_row['filename']

    img = get_image(filepath)

    buffer = BytesIO()
    img.save(buffer, format='png')

    result_dict = {
        'img': "data:image/png;base64," + base64.b64encode(buffer.getvalue()).decode(),
        'annotatedData': {
            'width': img.width,
            'height': img.height,
            'data': extract_words(img, app.config['TESSERACT_CONFIG'])
        }
    }
    logger.debug(result_dict)

    return jsonify(result_dict)


if __name__ == '__main__':
    app.secret_key = os.urandom(24)
    init_db()

    class_model = load_model(app.config['CLASSIFY_MODEL_PATH'])

    app.run(debug=True, host='0.0.0.0')
